'use strict';

const express = require('express');
const router = express.Router();

const TaskService = require('../service/TaskService');
const taskService = new TaskService();

// Get every task
router.get('/', (req, res) => {
    try {
        res.json(taskService.getTasks());
    } catch (error) {
        return res.status(error.code).json({
            status: 'error',
            message: error.message
        });
    }
});

// Get task by Id
router.get('/:id', (req, res) => {
    try {
        res.json(taskService.getTaskById(+req.params.id));
    } catch (error) {
        return res.status(error.code).json({
            status: 'error',
            message: error.message
        });
    }
});

// Create task
router.post('/:id', async (req, res) => {
    try {
        res.status(201).json(await taskService.createTask(
            +req.params.id,
            req.body.title,
            req.body.dateBegin,
            req.body.dateEnd,
            req.body.status,
            req.body.tags
        ));
    } catch (error) {
        return res.status(error.code).json({
            status: 'error',
            message: error.message
        });
    }
});

// Update task
router.patch('/:id', async (req, res) => {
    try {
        res.json(await taskService.updateTask(
            +req.params.id,
            req.body.title,
            req.body.dateBegin,
            req.body.dateEnd,
            req.body.status,
            req.body.tags
        ));
    } catch (error) {
        return res.status(error.code).json({
            status: 'error',
            message: error.message
        });
    }
});

// Delete task
router.delete('/:id', async (req, res) => {
    try {
        res.json(taskService.deleteTaskById(+req.params.id));
    } catch (error) {
        return res.status(error.code).json({
            status: 'error',
            message: error.message
        });
    }
});

module.exports = router;