'use strict';

const Task = require('../model/Task');
const Db = require('../common/db');
const db = new Db();

const TagService = require('./TagService');
const tagService = new TagService();

module.exports = class TaskService { 

    // Return every task
    getTasks() {
        const tasks = db.getAll('tasks');
        return tasks;
    }

    // Get a task with its Id
    getTaskById(id) {
        const task = db.getBy('tasks', { id });
        if (!task) throw { code: 500, message: "task does not exist" };
        return task;
    }

    // Create a task
    async createTask(id, title, dateBegin, dateEnd, status, tags) {

        // Stop creation if an argument is missing 
        if (!id || !title || !dateBegin || !dateEnd || !status || !tags) throw { code: 400, message: "required parameters missing" };

        // Refuse creation if tags are not an array
        if (!Array.isArray(tags)) throw { code: 400, message: "'tags' parameter is not an array" };
        
        // Check if task already exists
        const task = db.getBy('tasks', { id });
        if (task) throw { code: 500, message: "task already exists" };

        // Check if tags are available
        let unavailableTags = "";
        for(let i = 0; i < tags.length; i++) {
            try {
                const tag = await tagService.getTagByLabel(tags[i]);
                tags[i] = tag.id;
            } catch (error) {
                unavailableTags += `"${tags[i]}", `
            }
        }
        if(unavailableTags != "") throw { code: 500, message: `tags: ${unavailableTags} do(es) not exist`};

        // Add new task
        const newTask = new Task(id, title, dateBegin, dateEnd, status, tags ? tags : []);
        db.insert('tasks', newTask);
        return { newTask };
    }

    // Update a specific task
    async updateTask(id, title, dateBegin, dateEnd, status, tags) {
        // Stop creation if an argument is missing 
        if (!id) throw { code: 400, message: "'tags' parameter is not an array" };

        // Refuse creation if tags are not an array
        if (tags && !Array.isArray(tags)) throw { code: 400, message: "'tags' parameter is not an array" };

        // Check if tags are available
        let unavailableTags = "";
        for(let i = 0; i < tags.length; i++) {
            try {
                const tag = await tagService.getTagByLabel(tags[i]);
                tags[i] = tag.id;
            } catch (error) {
                unavailableTags += `"${tags[i]}", `
            }
        }
        if(unavailableTags != "") throw { code: 500, message: `tags: ${unavailableTags} do(es) not exist`};

        // Check if task already exists
        const task = db.getBy('tasks', { id });
        if (!task) throw { code: 500, message: "task does not exist" };

        // check if dateBegin is valid
        var timestampB = Date.parse(dateBegin);
        if (isNaN(timestampB) == false) {
            dateBegin = new Date(timestampB);
        } else {
            throw {code: 500, message: "dateBegin is not a date"}
        }

        // check if dateEnd is valid
        var timestampE = Date.parse(dateBegin);
        if (isNaN(timestampB) == false) {
            dateEnd = new Date(timestampE);
        } else {
            throw {code: 500, message: "dateEnd is not a date"}
        }

        // Update task
        db.update('tasks', { id }, {
            title,
            dateBegin,
            dateEnd,
            status,
            tags
        });
        return { task };
    }

    // Delete a task from with its Id
    deleteTaskById(id) {
        const task = db.getBy('tasks', { id });
        if (task) {
            db.remove('tasks', id);
            return { task };
        } else {
            throw { code: 500, message: "task noes not exist" };
        }
    }

};