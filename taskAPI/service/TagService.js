'use strict';

const Tag = require('../model/Tag');
const axios = require('axios');

module.exports = class TagService {

    // Return every tags
    async getTags() {
        let res;
        await axios.get('http://localhost:9090/api/tags').then(function (response) {
            res = response.data;
        }).catch(function (error) {
            throw { code: 500, message: "cannot access to tags" };
        });
        return res;
    }

    // Get a tags with its Id
    async getTagById(id) {
        let res;
        await axios.get(`http://localhost:9090/api/tags/${id}`).then(function (response) {
            res = response.data;
        }).catch(function (error) {
            console.log(error)
            throw { code: error.response.status, message: error.response.data.message };
        });
        if (!res) throw { code: 500, message: "empty tag received" };
        return res;
    }

    // Get a tags with its Id
    async getTagByLabel(label) {
        let res;
        await axios.get(`http://localhost:9090/api/tags/?label=${label}`).then(function (response) {
            res = response.data;
            console.log("1")
        }).catch(function (error) {
            console.log(error)
            throw { code: error.response.status, message: error.response.data.message };
        });
        if (!res) throw { code: 500, message: "empty tag received" };
        return res;
    }

};