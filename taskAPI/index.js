'use strict';

const express = require('express');
const app = express();
const bodyParser = require('body-parser');

const logMiddleware = require('./middleware/logMiddleware.js');

const taskRouter = require('./router/taskRouter');

// parse application/json
app.use(bodyParser.json())

// Log middleware
app.use(logMiddleware());

// Routers
app.use('/api/tasks', taskRouter);

// Start server
app.listen('8080', () => {
    console.log(`App running on port 8080!`);
});
