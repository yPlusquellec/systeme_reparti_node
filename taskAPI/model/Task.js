module.exports = class Task {

    constructor(id, title, dateBegin, dateEnd, status, tags) {
        this.id = id;
        this.title = title;

        var timestampB = Date.parse(dateBegin);
        if (isNaN(timestampB) == false) {
            this.dateBegin = new Date(timestampB);
        } else {
            throw {code: 500, message: "dateBegin is not a date"}
        }
        
        var timestampE = Date.parse(dateEnd);
        if (isNaN(timestampE) == false) {
            this.dateEnd = new Date(timestampE);
        } else {
            throw {code: 500, message: "dateEnd is not a date"}
        }
        this.status = status;
        this.tags = tags;
    }
    
} 