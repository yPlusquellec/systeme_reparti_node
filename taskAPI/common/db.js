'use strict';

const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const Task = require('../model/task');
const fs = require('fs');
const logger = require('./util/logger');

const RESET_DB = true;

module.exports = class Db {
    constructor() {
        // Reset database if set in config file
        if (RESET_DB) {
            logger.info('Resetting db');
            fs.unlinkSync('db.json');
        }

        // Chargement du fichier
        this.adapter = new FileSync('db.json');
        this.db = low(this.adapter);

        // Initialisation de la base de donnée si fichier vide
        this.db.defaults({
            tasks: [new Task(1, "Finir le projet JEE", "2018-03-20", "2018-04-11", "non précisé", [1, 3, 2]),
                new Task(2, "Finir le projet réseau", "2018-03-20", "2018-04-11", "non précisé", [7, 5, 2]),
                new Task(3, "Finir le projet Java", "2018-03-20", "2018-04-11", "non précisé", [2, 3])
            ]
        }).write();
    }

    // fonction d'insertion dans la bdd
    insert(table, object) {
        logger.info('Inserting value in ' + table);
        this.db.get(table).push(object).write();
    }

    // fonction de récupération d'une valeur de la bdd
    getBy(table, conditionObject) {
        logger.info('Getting one value from ' + table);
        return this.db.get(table).find(conditionObject).value();
    }

    // fonction de récupération de valeurs de la bdd
    getAllBy(table, conditionObject) {
        logger.info('Get some values from ' + table);
        return this.db.get(table).filter(conditionObject).value();
    }

    // Fonction de mise à jour d'une valeur de la bdd
    update(table, conditionObject, values) {
        this.db.get(table)
            .find(conditionObject)
            .assign(values)
            .write();
    }

    // fonction de suppression d'une valeur de la bdd
    remove(table, id) {
        logger.info('Removing value from ' + table);
        this.db.get(table).remove({
            id
        }).write();
    }

    // fonction de récupération de toutes les valeur de la bdd
    getAll(table) {
        logger.info('Getting every values ' + table);
        return this.db.get(table).value();
    }
};
