# Projet REST NodeJS

Le projet est séparé en 2 API. Une API "tasks" et une API "tags".   
Ces deux API communiquent ensemble afin de pouvoir utiliser les **tags** dans les **tasks**.

## Les API

### API Tasks

#### Installation
---

* Pour lancer l'API **tasks**, il est nécessaire d'aller dans le répertoire `taskAPI` et lancer les commandes.
```sh
npm install
npm start
```

#### Accès
---

L'API Tasks est disponible sur votre adresse [localhost:8080/api/tasks](localhost:8080/api/tasks).  
Il existe plusieurs verbes d'API mis en place pour accéder aux données :  
* GET : [localhost:8080/api/tasks](localhost:8080/api/tasks) pour obtenir la liste des tâches générées.
* GET : [localhost:8080/api/tasks/:id](localhost:8080/api/tasks/:id) pour accéder à une tâche précise en fonction de son id.
* POST : [localhost:8080/api/tasks/:id](localhost:8080/api/tasks/:id) pour créer une nouvelle tâche en fonction de l'id renseigné dans l'url et d'un json de la forme
```json
{
    "title": "Finir le projet JEE",
    "dateBegin": "2018-03-21",
    "dateEnd": "2018-04-12",
    "status": "non précisé",
    "tags": [
        "youpi",
        "travail",
        "web"
    ]
}
```
* PATCH : [localhost:8080/api/tasks/:id](localhost:8080/api/tasks/:id) pour mettre à jour une tâche existante en fonction de l'id renseigné dans l'url et d'un json de la forme
```json
{
	"title": "Finir le projet UML",
    "dateBegin": "2018-03-20",
    "dateEnd": "2018-04-11",
    "status": "non précisé",
    "tags": [
        "youpi",
        "travail",
        "à faire absolument"
    ]
}
```
* DELETE : [localhost:8080/api/tasks/:id](localhost:8080/api/tasks/:id) pour supprimer une tâche précise en fonction de son id.  

**Important : Il est nécessaire d'envoyer le label des tags pour la création ou la modification d'une tâche.**  
**Ce choix à été fait pour permettre un meilleure ergonomie à l'utilisateur lors de son ajout.**  
**Ce sera toutefois bien l'id qui sera enregistrée en base de donnée.**


### API Tags

#### Installation
---

* Pour lancer l'API **tasks**, il est nécessaire d'aller dans le répertoire `taskAPI` et lancer les commandes.
```sh
npm install
npm start
```

#### Accès
---

L'API Tags est disponible sur votre adresse [localhost:9090/api/tags](localhost:9090/api/tags).  
Il existe plusieurs verbes d'API mis en place pour accéder aux données :  
* GET : [localhost:9090/api/tags](localhost:9090/api/tags) pour obtenir la liste des tags générés.
* GET : [localhost:9090/api/tags/:id](localhost:9090/api/tags/:id) pour accéder à un tag précis en fonction de son id.
* GET : [localhost:9090/api/tags/?label=](localhost:9090/api/tags/?label=) pour accéder à un tag précis en fonction de son label.
* POST : [localhost:9090/api/tags/:id](localhost:9090/api/tags/:id) pour créer un nouveau tag en fonction de l'id renseigné dans l'url et d'un json de la forme
```json
{
	"label": "important"
}
```
* PATCH : [localhost:9090/api/tags/:id](localhost:9090/api/tags/:id) pour mettre à jour un tag existant en fonction de l'id renseigné dans l'url et d'un json de la forme
```json
{
	"label": "plus si important"
}
```
* DELETE : [localhost:9090/api/tags/:id](localhost:9090/api/tags/:id) pour supprimer un tag précis en fonction de son id.  

## Les bases de données

* Les données stockées pour chaque base sont dans le fichier db.json, à la racine de chaque API.
* Les bases de données sont automatiquement vidées puis reremplies avec des valeurs par défaut à chaque redémarrage.