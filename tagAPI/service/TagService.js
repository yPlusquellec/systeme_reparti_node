'use strict';

const Tag = require('../model/Tag');
const Db = require('../common/db');
const db = new Db();

module.exports = class TagService {

    // Return every tags
    getTags() {
        return db.getAll('tags');
    }

    // Get a tags with its Id
    getTagById(id) {
        const tag = db.getBy('tags', { id });
        if (!tag) throw { code: 500, message: "tag does not exist" };
        return tag;
    }

    // Get a tags with its label
    getTagByLabel(label) {
        const tag = db.getBy('tags', { label });
        if (!tag) throw { code: 500, message: "tag does not exist" };
        return tag;
    }

    // Create a tag
    createTag(id, label) {
        // Stop creation if an argument is missing 
        if (!id || !label) throw { code: 400, message: "required parameters missing" };

        // Check if tag already exists
        const tag = db.getBy('tags', { id });
        if (tag) throw { code: 500, message: "tag already exists" };

        // Creation of the tag
        const newTag = new Tag(id, label);
        db.insert('tags', newTag);
        return { newTag };
    }

    // Update a specific tag
    updateTag(id, label) {
        // Stop creation if an argument is missing 
        if (!id || !label) throw { code: 400, message: "required parameters missing" };

        // Check if tag exists
        const tag = db.getBy('tags', { id });
        if (!tag) throw { code: 500, message: "task does not exist" };

        // Update tag
        db.update('tags', { id }, { label });
        return { tag };
    }

    // Delete a tag from with its Id
    deleteTagById(id) {
        const tag = db.getBy('tags', { id });
        if(tag) {
            db.remove('tags', id);
            return { tag };
        } else {
            throw { code: 500, message: "tag noes not exist" };
        }
    }

};