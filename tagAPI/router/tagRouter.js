'use strict';

const express = require('express');
const router = express.Router();

const TagService = require('../service/tagService');
const tagService = new TagService();

// Get every tag
router.get('/', (req, res) => {
    
    if (req.query.label) { // get by label
        try {
            res.json(tagService.getTagByLabel(req.query.label));
        } catch (error) {
            return res.status(error.code).json({
                status: 'error',
                message: error.message
            });
        }
    } else { // get every tags
        try {
            res.json(tagService.getTags());
        } catch (error) {
            return res.status(error.code).json({
                status: 'error',
                message: error.message
            });
        }
    }
});

// Get tag by Id
router.get('/:id', (req, res) => {
    try {
        res.json(tagService.getTagById(+req.params.id));
    } catch (error) {
        return res.status(error.code).json({
            status: 'error',
            message: error.message
        });
    }
});

// Create tag
router.post('/:id', (req, res) => {
    try {
        res.status(201).json(tagService.createTag(
            +req.params.id,
            req.body.label,
        ));
    } catch (error) {
        return res.status(error.code).json({
            status: 'error',
            message: error.message
        });
    }
});

// Update tag
router.patch('/:id', (req, res) => {
    try {
        res.json(tagService.updateTag(
            +req.params.id,
            req.body.label,
        ));
    } catch (error) {
        return res.status(error.code).json({
            status: 'error',
            message: error.message
        });
    }
});

// Delete tag
router.delete('/:id', (req, res) => {
    try {
        res.json(tagService.deleteTagById(+req.params.id));
    } catch (error) {
        return res.status(error.code).json({
            status: 'error',
            message: error.message
        });
    }
});

module.exports = router;