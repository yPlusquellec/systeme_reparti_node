'use strict';

const express = require('express');
const app = express();
const bodyParser = require('body-parser');

const logMiddleware = require('./middleware/logMiddleware.js');

const tagRouter = require('./router/tagRouter');

// parse application/json
app.use(bodyParser.json())

// Log middleware
app.use(logMiddleware());

// Routers
app.use('/api/tags', tagRouter);

// Start server
app.listen('9090', () => {
    console.log(`App running on port 9090!`);
});
