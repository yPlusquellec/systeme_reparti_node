'use strict';

const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const Tag = require('../model/tag');
const fs = require('fs');
const logger = require('./util/logger');

const RESET_DB = true;

module.exports = class Db {
    constructor() {
        // Reset database if set in config file
        if (RESET_DB) {
            logger.info('Resetting db');
            fs.unlinkSync('db.json');
        }

        // Chargement du fichier
        this.adapter = new FileSync('db.json');
        this.db = low(this.adapter);

        // Initialisation de la base de donnée si fichier vide
        this.db.defaults({
            tags: [
                new Tag(1, "youpi"),
                new Tag(2, "web"),
                new Tag(3, "travail"),
                new Tag(5, "EZPZ"),
                new Tag(6, "Java"),
                new Tag(7, "à faire absolument")
            ]
        }).write();
    }

    // fonction d'insertion dans la bdd
    insert(table, object) {
        logger.info('Inserting value in ' + table);
        this.db.get(table).push(object).write();
    }

    // fonction de récupération d'une valeur de la bdd
    getBy(table, conditionObject) {
        logger.info('Getting one value from ' + table);
        return this.db.get(table).find(conditionObject).value();
    }

    // fonction de récupération de valeurs de la bdd
    getAllBy(table, conditionObject) {
        logger.info('Get some values from ' + table);
        return this.db.get(table).filter(conditionObject).value();
    }

    // Fonction de mise à jour d'une valeur de la bdd
    update(table, conditionObject, values) {
        this.db.get(table)
            .find(conditionObject)
            .assign(values)
            .write();
    }

    // fonction de suppression d'une valeur de la bdd
    remove(table, id) {
        logger.info('Removing value from ' + table);
        this.db.get(table).remove({
            id
        }).write();
    }

    // fonction de récupération de toutes les valeur de la bdd
    getAll(table) {
        logger.info('Getting every values ' + table);
        return this.db.get(table).value();
    }
};
