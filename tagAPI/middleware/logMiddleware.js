'use strict';

const logger = require('../common/util/logger');

module.exports = () => {
  return (req, _, next) => {
    logger.debug(`Request URL : [${req.method}] ${req.originalUrl}`);
    next();
  };
};
